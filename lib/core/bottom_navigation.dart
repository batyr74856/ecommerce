import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../feature/presentation/cubit/nav_cubit.dart';
import 'app_enums.dart';

class BottomNavigation extends StatelessWidget with PreferredSizeWidget {
  final BottomNavIndex? currentNavIndex;

  const BottomNavigation({Key? key, this.currentNavIndex}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavCubit, NavState>(builder: (context, state) {
      return WillPopScope(
        onWillPop: () async {
          if (currentNavIndex != BottomNavIndex.home) {
            return false;
          } else {
            return true;
          }
        },
        child: SizedBox(
          height: 72,
          child: Stack(
            children: [
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 72,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    color: Color.fromRGBO(
                      1,
                      0,
                      53,
                      1,
                    ),
                  ),
                ),
              ),
              Positioned(
                  bottom: 8,
                  top: 0,
                  right: 0,
                  left: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 8,
                            width: 8,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30)),
                          ),
                          const SizedBox(
                            width: 7,
                          ),
                          const Text(
                            'Explorer',
                            style: TextStyle(
                                fontFamily: 'Mark Pro',
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 15),
                          )
                        ],
                      ),
                      BottomNavigationItem(
                        active: currentNavIndex == BottomNavIndex.home,
                        asset: BottomNavIndex.home.asset,
                        onClick: () {
                          context
                              .read<NavCubit>()
                              .changeNav(BottomNavIndex.home);
                        },
                      ),
                      BottomNavigationItem(
                        active: currentNavIndex == BottomNavIndex.favorite,
                        asset: BottomNavIndex.favorite.asset,
                        onClick: () {
                          context
                              .read<NavCubit>()
                              .changeNav(BottomNavIndex.favorite);
                        },
                      ),
                      BottomNavigationItem(
                        active: currentNavIndex == BottomNavIndex.profile,
                        asset: BottomNavIndex.profile.asset,
                        onClick: () {
                          context
                              .read<NavCubit>()
                              .changeNav(BottomNavIndex.profile);
                        },
                      ),
                    ],
                  ))
            ],
          ),
        ),
      );
    });
  }

  @override
  Size get preferredSize => const Size.fromHeight(60);
}

class BottomNavigationItem extends StatelessWidget {
  final bool active;
  final String asset;
  final Function()? onClick;

  const BottomNavigationItem(
      {Key? key, this.active = false, required this.asset, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (active) {
      return GestureDetector(
        onTap: onClick,
        child: SvgPicture.asset(
          asset,
          color: const Color.fromRGBO(255, 110, 78, 1),
        ),
      );
    }
    return InkWell(
      onTap: onClick,
      child: Ink(
          width: 44,
          height: 44,
          padding: const EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [SvgPicture.asset(asset)],
          )),
    );
  }
}
