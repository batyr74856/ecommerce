import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_animation/skeleton_animation.dart';

class BaseNetworkImage extends StatelessWidget {
  final String? url;
  final BoxFit? fit;

  const BaseNetworkImage({
    Key? key,
    this.url,
    this.fit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (url == null) {
      return const SizedBox();
    }
    if (url!.contains('base64')) {
      return Image.memory(
        base64Decode(url!),
        fit: fit,
      );
    }
    return CachedNetworkImage(
      imageUrl: url!,
      fit: fit,
      errorWidget: (_, __, ___) => const ImagePlaceholder(),
      placeholder: (_, message) => Skeleton(
        width: double.infinity,
        height: double.infinity,
      ),
    );
  }
}

class ImagePlaceholder extends StatelessWidget {
  const ImagePlaceholder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      child: Center(
        child: Icon(Icons.image_not_supported_outlined),
      ),
    );
  }
}
