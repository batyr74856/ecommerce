import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class ContentSlider extends StatefulWidget {
  final double height;
  final int itemCount;
  final bool autoPlay;
  final Widget Function(BuildContext context, int itemIndex, int pageViewIndex)?
      itemBuilder;
  final Function(int index, CarouselPageChangedReason reason)? onPageChanged;

  const ContentSlider(
      {Key? key,
      this.height = 200,
      this.itemBuilder,
      this.itemCount = 0,
      this.onPageChanged,
      this.autoPlay = true})
      : super(key: key);

  @override
  State<ContentSlider> createState() => _ContentSliderState();
}

class _ContentSliderState extends State<ContentSlider> {
  String currentKey = '';
  final CarouselController controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CarouselSlider.builder(
          carouselController: controller,
          itemCount: widget.itemCount,
          itemBuilder: widget.itemBuilder != null
              ? (context, index, pageViewIndex) {
                  return Stack(
                    children: [
                      widget.itemBuilder!(context, index, pageViewIndex),
                    ],
                  );
                }
              : null,
          options: CarouselOptions(
            height: widget.height,
            autoPlay: widget.autoPlay,
            viewportFraction: 1,
            enlargeCenterPage: true,
            scrollDirection: Axis.horizontal,
            onPageChanged: widget.onPageChanged,
          ),
        ),
      ],
    );
  }
}
