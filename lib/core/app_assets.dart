class AppAssets {
  AppAssets._();

  static const String location = 'assets/icons/location.svg';
  static const String arrowDown = 'assets/icons/arrow_down.svg';
  static const String filter = 'assets/icons/filter.svg';
  static const String phones = 'assets/icons/phones.svg';
  static const String computer = 'assets/icons/computer.svg';
  static const String health = 'assets/icons/health.svg';
  static const String books = 'assets/icons/books.svg';
  static const String qr = 'assets/icons/qr.svg';
  static const String search = 'assets/icons/search.svg';
  static const String heart = 'assets/icons/heart.svg';
  static const String arrowBack = 'assets/icons/arrow_back.svg';
  static const String basket = 'assets/icons/home.svg';
  static const String star = 'assets/icons/star.svg';
  static const String cpu = 'assets/icons/cpu.svg';
  static const String camera = 'assets/icons/camera.svg';
  static const String ram = 'assets/icons/ram.svg';
  static const String memory = 'assets/icons/memory.svg';
  static const String select = 'assets/icons/select.svg';
  static const String delete = 'assets/icons/delete.svg';
  static const String isFavorite = 'assets/icons/isFavorite.svg';
  static const String close = 'assets/icons/close.svg';
}
