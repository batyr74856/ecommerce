enum BottomNavIndex {
  home,
  favorite,
  profile,
}

extension BottomNavIndexExt on BottomNavIndex {
  static const keyMap = {
    BottomNavIndex.home: 'home',
    BottomNavIndex.favorite: 'favorite',
    BottomNavIndex.profile: 'profile',
  };

  static const assetMap = {
    BottomNavIndex.home: 'assets/icons/home.svg',
    BottomNavIndex.favorite: 'assets/icons/favorite.svg',
    BottomNavIndex.profile: 'assets/icons/profile.svg',
  };

  String get key => keyMap[this] ?? '';
  String get asset => assetMap[this] ?? '';
}
