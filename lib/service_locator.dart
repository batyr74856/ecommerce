import 'package:dio/dio.dart';
import 'package:ecommerce/feature/data/repositories/repository_impl.dart';
import 'package:ecommerce/feature/domain/repositories/product_repository.dart';

import 'package:ecommerce/feature/presentation/bloc/cart_bloc/cart_bloc.dart';
import 'package:ecommerce/feature/presentation/bloc/product_bloc/product_bloc.dart';
import 'package:ecommerce/feature/presentation/bloc/product_detail_bloc/product_detail_bloc.dart';
import 'package:ecommerce/feature/presentation/cubit/nav_cubit.dart';
import 'package:get_it/get_it.dart';

import 'feature/data/datasources/remote/rest_client.dart';
import 'core/env.dart';

import 'feature/domain/repositories/cart_repository.dart';
import 'feature/domain/repositories/product_detail_repository.dart';
import 'feature/domain/usecases/get_all_products.dart';
import 'feature/domain/usecases/get_cart_products.dart';
import 'feature/domain/usecases/get_detail_product.dart';
import 'utils/shared_preference.dart';

GetIt serviceLocator = GetIt.instance;

Future<void> serviceLocatorSetup() async {
  serviceLocator
    ..registerFactory(() => NavCubit())
    ..registerFactory(() => ProductBloc())
    ..registerFactory(() => ProductDetailBloc())
    ..registerFactory(() => CartBloc())
    ..registerLazySingleton<RepositoriesService>(() => ServiceImpl())
    ..registerLazySingleton<ProductRepository>(
        () => ProductRepositoryImp(repositoriesService: serviceLocator()))
    ..registerLazySingleton<ProductDetailRepository>(
        () => ProductDetailRepositoryImp(repositoriesService: serviceLocator()))
    ..registerLazySingleton<CartRepository>(
        () => CartRepositoryImp(repositoriesService: serviceLocator()))
    ..registerFactory<RestClient>(() {
      final dio = Dio();

      return RestClient(dio, baseUrl: apiURL);
    });

  serviceLocator.registerLazySingletonAsync<SharedPreferenceHelper>(() async {
    final SharedPreferenceHelper storage = SharedPreferenceHelper();
    await storage.init();
    return storage;
  });
}
