import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:ecommerce/feature/data/models/product_detail.dart';

abstract class ProductDetailRepository {
  Future<Either<DioError, ProductDetail>> getProductDetail();
}
