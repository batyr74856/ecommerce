import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../../data/models/cart.dart';

abstract class CartRepository {
  Future<Either<DioError, Cart>> getCart();
}
