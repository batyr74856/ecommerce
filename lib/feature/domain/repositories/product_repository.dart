import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import 'package:ecommerce/feature/data/models/product.dart';

abstract class ProductRepository {
  Future<Either<DioError, Product>> getProductList();
}
