import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:ecommerce/feature/data/models/cart.dart';
import '../../data/repositories/repository_impl.dart';
import '../repositories/cart_repository.dart';

class CartRepositoryImp extends CartRepository {
  CartRepositoryImp({
    required RepositoriesService repositoriesService,
  }) : _repositoriesService = repositoriesService;

  final RepositoriesService _repositoriesService;

  @override
  Future<Either<DioError, Cart>> getCart() async {
    return await _repositoriesService.getCart();
  }
}
