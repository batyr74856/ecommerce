import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:ecommerce/feature/data/models/cart.dart';
import 'package:ecommerce/feature/data/models/product.dart';
import 'package:ecommerce/feature/data/models/product_detail.dart';
import 'package:ecommerce/feature/domain/repositories/product_repository.dart';

import '../../data/repositories/repository_impl.dart';

class ProductRepositoryImp extends ProductRepository {
  ProductRepositoryImp({
    required RepositoriesService repositoriesService,
  }) : _repositoriesService = repositoriesService;

  final RepositoriesService _repositoriesService;

  @override
  Future<Either<DioError, Product>> getProductList() async {
    return await _repositoriesService.getProductList();
  }
}
