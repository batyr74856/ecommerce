import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:ecommerce/feature/data/models/product_detail.dart';
import '../../data/repositories/repository_impl.dart';
import '../repositories/product_detail_repository.dart';

class ProductDetailRepositoryImp extends ProductDetailRepository {
  ProductDetailRepositoryImp({
    required RepositoriesService repositoriesService,
  }) : _repositoriesService = repositoriesService;

  final RepositoriesService _repositoriesService;

  @override
  Future<Either<DioError, ProductDetail>> getProductDetail() async {
    return await _repositoriesService.getProductDetail();
  }
}
