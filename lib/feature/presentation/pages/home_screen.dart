import 'package:ecommerce/feature/presentation/widgets/home_widgets/best_seller.dart';
import 'package:ecommerce/feature/presentation/widgets/home_widgets/home_app_bar.dart';
import 'package:ecommerce/feature/presentation/widgets/home_widgets/hot_sales.dart';
import 'package:ecommerce/feature/presentation/widgets/home_widgets/search_product.dart';
import 'package:ecommerce/feature/presentation/widgets/home_widgets/select_category.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int activeTab = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(248, 248, 248, 1),
      appBar: PreferredSize(
          preferredSize: const Size(double.infinity, 50), child: HomeAppBar()),
      body: ListView(
        children: [
          SelectCategoryWidget(
            activeTab: activeTab,
          ),
          const SizedBox(
            height: 24,
          ),
          const SearchWidget(),
          const SizedBox(
            height: 24,
          ),
          const HotSalesWidget(),
          const SizedBox(
            height: 24,
          ),
          const BestSellerWidget(),
          const SizedBox(
            height: 120,
          )
        ],
      ),
    );
  }
}
