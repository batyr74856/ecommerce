import 'package:ecommerce/feature/presentation/widgets/cart_widgets/cart_app_bar.dart';
import 'package:flutter/material.dart';

import '../widgets/cart_widgets/cart_body.dart';

// ignore: must_be_immutable
class CartPage extends StatelessWidget {
  CartPage({Key? key}) : super(key: key);
  int amount = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          Column(
            children: [
              const AppBarWidget(),
              const SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Row(
                  children: const [
                    Text(
                      'My Cart',
                      style: TextStyle(
                          fontFamily: 'Mark Pro',
                          fontSize: 35,
                          fontWeight: FontWeight.w700,
                          color: Color.fromRGBO(
                            1,
                            0,
                            53,
                            1,
                          )),
                    ),
                  ],
                ),
              ),
            ],
          ),
          CartBody(amount: amount)
        ],
      )),
    );
  }
}
