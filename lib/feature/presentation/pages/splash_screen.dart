import 'dart:async';

import 'package:flutter/material.dart';

import 'main_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        const Duration(seconds: 2),
        () => Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => const MainScreen(),
            ),
            ModalRoute.withName("/FirstScreen")));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(
        1,
        0,
        53,
        1,
      ),
      body: SafeArea(
        child: Center(
            child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: double.infinity,
              width: double.infinity,
              color: const Color.fromRGBO(
                1,
                0,
                53,
                1,
              ),
            ),
            Container(
              height: 132,
              width: 132,
              decoration: BoxDecoration(
                  color: const Color.fromRGBO(
                    255,
                    110,
                    78,
                    1,
                  ),
                  borderRadius: BorderRadius.circular(80)),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Padding(
                  padding: EdgeInsets.only(left: 100),
                  child: Text(
                    'Ecommerce',
                    style: TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 30,
                        fontWeight: FontWeight.w800,
                        color: Colors.white),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 100),
                  child: Text(
                    'Concept',
                    style: TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 30,
                        fontWeight: FontWeight.w800,
                        color: Colors.white),
                  ),
                ),
              ],
            ),
          ],
        )),
      ),
    );
  }
}
