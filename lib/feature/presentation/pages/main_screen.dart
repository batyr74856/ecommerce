import 'package:ecommerce/core/app_enums.dart';
import 'package:ecommerce/core/bottom_navigation.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/nav_cubit.dart';
import 'favorite_screen.dart';
import 'home_screen.dart';
import 'profile_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavCubit, NavState>(
      builder: (context, state) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          body: Stack(
            children: [
              SafeArea(
                child: Stack(
                  children: [
                    Offstage(
                      offstage: state.navIndex != BottomNavIndex.home,
                      child: const HomePage(),
                    ),
                    Offstage(
                      offstage: state.navIndex != BottomNavIndex.favorite,
                      child: const FavoriteScreen(),
                    ),
                    Offstage(
                      offstage: state.navIndex != BottomNavIndex.profile,
                      child: const ProfileScreen(),
                    ),
                  ],
                ),
              ),
              Positioned(
                  left: 25,
                  right: 25,
                  bottom: 20,
                  child: BottomNavigation(
                    currentNavIndex: state.navIndex,
                  )),
            ],
          ),
        );
      },
    );
  }
}
