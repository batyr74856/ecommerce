import 'dart:convert';

import 'package:ecommerce/core/image_network.dart';
import 'package:ecommerce/feature/data/datasources/local/favorite_provider.dart';
import 'package:ecommerce/feature/data/models/product.dart';
import 'package:ecommerce/feature/presentation/widgets/home_widgets/is_favorite.dart';
import 'package:ecommerce/utils/shared_preference.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/app_assets.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  String? favorite;

  List<BestSeller>? favorites;
  FavoriteProvider favoriteProvider = FavoriteProvider();

  bool isLoading = true;

  @override
  void initState() {
    super.initState();

    sharedPreference.reload().then((value) {
      setState(() {
        favorite = sharedPreference.favorite;
        favorites = (json.decode(favorite ?? '[]') as List)
            .reversed
            .map((e) => BestSeller.fromJson(e))
            .toList();
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          favorite == null && isLoading
              ? const Center(
                  child: Center(
                      child: CircularProgressIndicator(
                  color: Colors.red,
                )))
              : favorite != null
                  ? ListView.builder(
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      itemCount: favorites!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 25, vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Material(
                                elevation: 10,
                                borderRadius: BorderRadius.circular(12),
                                child: Container(
                                  height: 95,
                                  width: 89,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(12)),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(12),
                                    child: BaseNetworkImage(
                                      url: favorites![index].picture,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 180,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      favorites![index].title!,
                                      style: const TextStyle(
                                          fontFamily: 'Mark Pro',
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.black),
                                    ),
                                    Text(
                                      '\$${favorites![index].price_without_discount}.00',
                                      style: const TextStyle(
                                          fontFamily: 'Mark Pro',
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600,
                                          color: Color.fromRGBO(
                                            255,
                                            110,
                                            78,
                                            1,
                                          )),
                                    ),
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  favoriteProvider.removeFavorite(
                                      favorites![index].title!,
                                      favorites![index].picture!,
                                      favorites![index].price_without_discount!,
                                      isFavorite);
                                },
                                child: SvgPicture.asset(
                                  AppAssets.delete,
                                  color: Colors.red,
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    )
                  : const Center(child: Text('Пусто')),
          const SizedBox(
            height: 100,
          ),
        ],
      ),
    ));
  }
}
