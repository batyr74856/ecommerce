import 'package:ecommerce/feature/presentation/widgets/product_detail_widgets/product_detail_app_bar.dart';
import 'package:ecommerce/feature/presentation/widgets/product_detail_widgets/product_detail_body.dart';
import 'package:ecommerce/feature/presentation/widgets/product_detail_widgets/product_detail_slider.dart';
import 'package:flutter/material.dart';

class ProductDetailPage extends StatelessWidget {
  const ProductDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        body: SafeArea(
            child: Stack(
          children: [
            Column(
              children: const [
                ProductDetailAppBar(),
                SizedBox(
                  height: 20,
                ),
                ProductDetailSlider(),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
            const ProductDetailBody()
          ],
        )),
      ),
    );
  }
}
