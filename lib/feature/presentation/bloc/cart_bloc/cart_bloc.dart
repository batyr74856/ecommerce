import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:ecommerce/feature/data/models/cart.dart';
import 'package:ecommerce/feature/domain/repositories/cart_repository.dart';
import 'package:ecommerce/service_locator.dart';

import 'package:equatable/equatable.dart';

import '../../../domain/repositories/product_repository.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartRepository repository = serviceLocator<CartRepository>();
  CartBloc() : super(CartInitial()) {
    on<GetCartEvent>(getCart);
  }

  FutureOr<void> getCart(GetCartEvent event, Emitter<CartState> emit) async {
    emit(CartLoading());
    final response = await repository.getCart();

    response.fold((left) => null, (right) => emit(CartLoaded(cart: right)));
  }
}
