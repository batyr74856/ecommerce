part of 'cart_bloc.dart';

abstract class CartState extends Equatable {}

class CartInitial extends CartState {
  @override
  List<Object> get props => [];
}

class CartLoading extends CartState {
  @override
  List<Object> get props => [];
}

class CartLoaded extends CartState {
  final Cart cart;
  CartLoaded({required this.cart});

  @override
  List<Object> get props => [];
}
