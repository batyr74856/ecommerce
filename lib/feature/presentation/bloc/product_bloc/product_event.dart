part of 'product_bloc.dart';

abstract class ProductEvent extends Equatable {}

class GetProduct extends ProductEvent {
  @override
  List<Object> get props => [];
}
