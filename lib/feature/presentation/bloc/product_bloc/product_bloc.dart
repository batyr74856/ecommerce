import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:ecommerce/feature/data/repositories/repository_impl.dart';

import 'package:equatable/equatable.dart';

import '../../../../service_locator.dart';
import '../../../data/models/product.dart';
import '../../../domain/repositories/product_repository.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  ProductRepository repository = serviceLocator<ProductRepository>();

  ProductBloc() : super(ProductInitial()) {
    on<GetProduct>(getProduct);
  }

  FutureOr<void> getProduct(
      GetProduct event, Emitter<ProductState> emit) async {
    emit(ProductLoading());
    final response = await repository.getProductList();

    response.fold(
        (left) => null, (right) => emit(ProductLoaded(product: right)));
  }
}
