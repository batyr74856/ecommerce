part of 'product_detail_bloc.dart';

abstract class ProductDetailEvent extends Equatable {
  const ProductDetailEvent();
}

class GetProductDetail extends ProductDetailEvent {
  @override
  List<Object> get props => [];
}
