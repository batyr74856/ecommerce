import 'dart:async';

import 'package:bloc/bloc.dart';

import 'package:ecommerce/service_locator.dart';

import 'package:equatable/equatable.dart';

import '../../../data/models/product_detail.dart';
import '../../../domain/repositories/product_detail_repository.dart';

part 'product_detail_event.dart';
part 'product_detail_state.dart';

class ProductDetailBloc extends Bloc<ProductDetailEvent, ProductDetailState> {
  ProductDetailRepository repositories =
      serviceLocator<ProductDetailRepository>();

  ProductDetailBloc() : super(ProductDetailInitial()) {
    on<GetProductDetail>(getProductDetail);
  }

  FutureOr<void> getProductDetail(
      GetProductDetail event, Emitter<ProductDetailState> emit) async {
    emit(ProductDetailLoading());
    final response = await repositories.getProductDetail();

    response.fold((left) => null,
        (right) => emit(ProductDetailLoaded(productDetail: right)));
  }
}
