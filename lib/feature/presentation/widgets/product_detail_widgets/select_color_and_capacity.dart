import 'package:ecommerce/core/app_assets.dart';
import 'package:ecommerce/core/hex_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SelectColorAndCapacityWidget extends StatefulWidget {
  const SelectColorAndCapacityWidget(
      {Key? key, required this.itemColor, required this.itemCapacity})
      : super(key: key);
  final List<String>? itemColor;
  final List<String>? itemCapacity;

  @override
  State<SelectColorAndCapacityWidget> createState() =>
      _SelectColorAndCapacityWidgetState();
}

int activeTabColor = 0;
int activeTabCapacity = 0;

class _SelectColorAndCapacityWidgetState
    extends State<SelectColorAndCapacityWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            height: 39,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: widget.itemColor!.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: () {
                    setState(() {
                      activeTabColor = index;
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 18),
                    child: Container(
                        height: 39,
                        width: 39,
                        padding: const EdgeInsets.all(12),
                        decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(33)),
                            color: HexColor(widget.itemColor![index])),
                        child: activeTabColor == index
                            ? SvgPicture.asset(AppAssets.select)
                            : const SizedBox()),
                  ),
                );
              },
            ),
          ),
          SizedBox(
            height: 39,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: widget.itemCapacity!.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: () {
                    setState(() {
                      activeTabCapacity = index;
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: Container(
                      height: 28,
                      width: 80,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(15)),
                          color: activeTabCapacity == index
                              ? const Color.fromRGBO(255, 110, 78, 1)
                              : Colors.white),
                      child: Text(
                        '${widget.itemCapacity![index]} gb',
                        style: TextStyle(
                            fontFamily: 'Mark Pro',
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            color: activeTabCapacity == index
                                ? Colors.white
                                : const Color.fromRGBO(
                                    141,
                                    141,
                                    141,
                                    1,
                                  )),
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
