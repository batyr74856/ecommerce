import 'package:ecommerce/core/app_assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

// ignore: must_be_immutable
class ShopWidget extends StatelessWidget {
  ShopWidget(
      {Key? key,
      required this.cpu,
      required this.camera,
      required this.sd,
      required this.ssd})
      : super(key: key);
  String? cpu, camera, ssd, sd;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 30,
        ),
        Padding(
          padding: const EdgeInsets.only(right: 40, left: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  SvgPicture.asset(AppAssets.cpu),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    cpu!,
                    style: const TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(183, 183, 183, 1)),
                  )
                ],
              ),
              Column(
                children: [
                  SvgPicture.asset(AppAssets.camera),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    camera!,
                    style: const TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(183, 183, 183, 1)),
                  )
                ],
              ),
              Column(
                children: [
                  SvgPicture.asset(AppAssets.ram),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    ssd!,
                    style: const TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(183, 183, 183, 1)),
                  )
                ],
              ),
              Column(
                children: [
                  SvgPicture.asset(AppAssets.memory),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    sd!,
                    style: const TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(183, 183, 183, 1)),
                  )
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}
