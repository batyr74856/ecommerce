import 'package:ecommerce/core/app_assets.dart';
import 'package:ecommerce/feature/presentation/bloc/product_detail_bloc/product_detail_bloc.dart';
import 'package:ecommerce/service_locator.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';

import 'select_color_and_capacity.dart';
import 'shop.dart';

class ProductDetailBody extends StatelessWidget {
  const ProductDetailBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      child: Material(
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30), topRight: Radius.circular(30)),
        elevation: 10,
        shadowColor: Colors.grey,
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: 430,
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            child: BlocProvider<ProductDetailBloc>(
              create: (context) =>
                  serviceLocator<ProductDetailBloc>()..add(GetProductDetail()),
              child: BlocBuilder<ProductDetailBloc, ProductDetailState>(
                builder: (context, state) {
                  if (state is ProductDetailInitial) {
                    return const SizedBox();
                  } else if (state is ProductDetailLoading) {
                    return const Center(
                      child: CircularProgressIndicator(
                          color: Color.fromRGBO(
                        255,
                        110,
                        78,
                        1,
                      )),
                    );
                  } else if (state is ProductDetailLoaded) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 25),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '${state.productDetail.title}',
                                style: const TextStyle(
                                    fontFamily: 'Mark Pro',
                                    fontSize: 24,
                                    fontWeight: FontWeight.w700,
                                    color: Color.fromRGBO(
                                      1,
                                      0,
                                      53,
                                      1,
                                    )),
                              ),
                              Container(
                                padding: const EdgeInsets.all(12),
                                height: 37,
                                width: 37,
                                decoration: BoxDecoration(
                                    color: const Color.fromRGBO(1, 0, 53, 1),
                                    borderRadius: BorderRadius.circular(10)),
                                child: SvgPicture.asset(
                                  AppAssets.heart,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          child: RatingBarIndicator(
                              rating: state.productDetail.rating!,
                              itemBuilder: (context, index) {
                                return SizedBox(
                                  height: 18,
                                  child: Padding(
                                    padding: const EdgeInsets.all(3.0),
                                    child: SvgPicture.asset(
                                      AppAssets.star,
                                    ),
                                  ),
                                );
                              }),
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: TabBar(
                            labelStyle: TextStyle(
                              fontFamily: 'Mark Pro',
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                            labelColor: Color.fromRGBO(
                              1,
                              0,
                              53,
                              1,
                            ),
                            unselectedLabelStyle: TextStyle(
                              fontFamily: 'Mark Pro',
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                            ),
                            unselectedLabelColor: Color.fromRGBO(
                              0,
                              0,
                              0,
                              0.5,
                            ),
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicatorWeight: 3,
                            indicatorColor: Color.fromRGBO(255, 110, 78, 1),
                            tabs: [
                              Tab(
                                text: 'Shop',
                              ),
                              Tab(
                                text: 'Detail',
                              ),
                              Tab(
                                text: 'Features',
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 100,
                          child: TabBarView(
                            children: [
                              ShopWidget(
                                cpu: state.productDetail.CPU,
                                camera: state.productDetail.camera,
                                ssd: state.productDetail.ssd,
                                sd: state.productDetail.sd,
                              ),
                              const Center(child: Text('Detail')),
                              const Center(child: Text('Features')),
                            ],
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 25),
                          child: Text(
                            'Select color and capacity',
                            style: TextStyle(
                                fontFamily: 'Mark Pro',
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                color: Color.fromRGBO(
                                  1,
                                  0,
                                  53,
                                  1,
                                )),
                          ),
                        ),
                        SelectColorAndCapacityWidget(
                          itemColor: state.productDetail.color!,
                          itemCapacity: state.productDetail.capacity,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Container(
                            height: 50,
                            padding: const EdgeInsets.symmetric(horizontal: 40),
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: const Color.fromRGBO(255, 110, 78, 1),
                                borderRadius: BorderRadius.circular(12)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text(
                                  'Add to Cart',
                                  style: TextStyle(
                                      fontFamily: 'Mark Pro',
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white),
                                ),
                                Text(
                                  '\$${state.productDetail.price}.00',
                                  style: const TextStyle(
                                      fontFamily: 'Mark Pro',
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                  } else {
                    return const SizedBox();
                  }
                },
              ),
            )),
      ),
    );
  }
}
