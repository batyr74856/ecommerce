import 'package:ecommerce/core/slider.dart';
import 'package:ecommerce/feature/presentation/bloc/product_detail_bloc/product_detail_bloc.dart';
import 'package:ecommerce/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/image_network.dart';

class ProductDetailSlider extends StatelessWidget {
  const ProductDetailSlider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        child: BlocProvider<ProductDetailBloc>(
      create: (context) =>
          serviceLocator<ProductDetailBloc>()..add(GetProductDetail()),
      child: BlocBuilder<ProductDetailBloc, ProductDetailState>(
        builder: (context, state) {
          if (state is ProductDetailInitial) {
            return const SizedBox();
          } else if (state is ProductDetailLoading) {
            return const Center(
                child: CircularProgressIndicator(color: Colors.white));
          } else if (state is ProductDetailLoaded) {
            return ContentSlider(
              itemCount: state.productDetail.images!.length,
              autoPlay: true,
              itemBuilder: (context, index, pageViewIndex) {
                return BaseNetworkImage(
                  url: state.productDetail.images![index],
                  fit: BoxFit.fill,
                );
              },
            );
          } else {
            return const SizedBox();
          }
        },
      ),
    ));
  }
}
