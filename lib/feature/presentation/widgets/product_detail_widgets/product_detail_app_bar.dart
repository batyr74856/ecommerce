import 'package:ecommerce/core/app_assets.dart';
import 'package:ecommerce/feature/presentation/bloc/cart_bloc/cart_bloc.dart';
import 'package:ecommerce/feature/presentation/pages/cart_screen.dart';
import 'package:ecommerce/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProductDetailAppBar extends StatelessWidget {
  const ProductDetailAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: const EdgeInsets.all(12),
              height: 37,
              width: 37,
              decoration: BoxDecoration(
                  color: const Color.fromRGBO(1, 0, 53, 1),
                  borderRadius: BorderRadius.circular(10)),
              child: SvgPicture.asset(AppAssets.arrowBack),
            ),
          ),
          const Text(
            'Product Detail',
            style: TextStyle(
                fontFamily: 'Mark Pro',
                fontSize: 18,
                fontWeight: FontWeight.w700,
                color: Color.fromRGBO(
                  1,
                  0,
                  53,
                  1,
                )),
          ),
          InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: ((context) => CartPage())));
            },
            child: Stack(
              children: [
                Container(
                  padding: const EdgeInsets.all(12),
                  height: 37,
                  width: 37,
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(255, 110, 78, 1),
                      borderRadius: BorderRadius.circular(10)),
                  child: SvgPicture.asset(AppAssets.basket),
                ),
                Positioned(
                  top: 2,
                  right: 4,
                  child: BlocProvider<CartBloc>(
                    create: (context) =>
                        serviceLocator<CartBloc>()..add(GetCartEvent()),
                    child: BlocBuilder<CartBloc, CartState>(
                      builder: (context, state) {
                        if (state is CartInitial) {
                          return const SizedBox();
                        } else if (state is CartLoading) {
                          return const Center(
                              child: CircularProgressIndicator(
                                  color: Color.fromRGBO(
                            255,
                            110,
                            78,
                            1,
                          )));
                        } else if (state is CartLoaded) {
                          return Text(
                            '${state.cart.basket!.length}',
                            style: const TextStyle(
                                fontFamily: 'Mark Pro',
                                fontSize: 12,
                                fontWeight: FontWeight.w700,
                                color: Colors.white),
                          );
                        } else {
                          return const SizedBox();
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
