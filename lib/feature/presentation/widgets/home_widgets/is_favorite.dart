import 'package:ecommerce/core/app_assets.dart';
import 'package:ecommerce/feature/data/datasources/local/favorite_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

bool isFavorite = true;

// ignore: must_be_immutable
class IsFavorite extends StatefulWidget {
  IsFavorite({
    Key? key,
    required this.image,
    required this.title,
    required this.price,
  }) : super(key: key);
  String? title, image;
  int? price;
  bool? isFavorite;

  @override
  State<IsFavorite> createState() => _IsFavoriteState();
}

FavoriteProvider favoriteProvider = FavoriteProvider();
SvgPicture favoriteSvg = SvgPicture.asset(AppAssets.heart);
SvgPicture isFavoriteSvg = SvgPicture.asset(AppAssets.isFavorite);

class _IsFavoriteState extends State<IsFavorite> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        favoriteProvider.saveFavorite(
            widget.title!, widget.image, widget.price!, isFavorite);
        setState(() {
          favoriteSvg = isFavoriteSvg;
        });
      },
      child: Material(
        elevation: 2,
        borderRadius: BorderRadius.circular(50),
        child: Container(
            padding: const EdgeInsets.all(6),
            height: 28,
            width: 28,
            decoration: BoxDecoration(
                color: const Color.fromRGBO(255, 255, 255, 1),
                borderRadius: BorderRadius.circular(50)),
            child: favoriteSvg),
      ),
    );
  }
}
