import 'package:ecommerce/core/image_network.dart';
import 'package:ecommerce/core/slider.dart';
import 'package:ecommerce/feature/presentation/bloc/product_bloc/product_bloc.dart';
import 'package:ecommerce/feature/presentation/pages/product_detail_screen.dart';
import 'package:ecommerce/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

class HotSalesWidget extends StatelessWidget {
  const HotSalesWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 25,
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    'Hot Sales',
                    style: TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 25,
                        fontWeight: FontWeight.w700,
                        color: Color.fromRGBO(
                          1,
                          0,
                          53,
                          1,
                        )),
                  ),
                  Text(
                    'see more',
                    style: TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(
                          255,
                          110,
                          78,
                          1,
                        )),
                  ),
                ])),
        const SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: SizedBox(
              height: 200,
              child: BlocProvider<ProductBloc>(
                create: (context) =>
                    serviceLocator<ProductBloc>()..add(GetProduct()),
                child: BlocBuilder<ProductBloc, ProductState>(
                  builder: (context, state) {
                    if (state is ProductInitial) {
                      return const SizedBox();
                    } else if (state is ProductLoading) {
                      return const Center(
                          child: CircularProgressIndicator(
                              color: Color.fromRGBO(
                        255,
                        110,
                        78,
                        1,
                      )));
                    } else if (state is ProductLoaded) {
                      return ContentSlider(
                        itemCount: state.product.home_store!.length,
                        autoPlay: true,
                        itemBuilder: (context, index, pageViewIndex) {
                          return Stack(
                            children: [
                              Container(
                                height: 200,
                                decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20)),
                                ),
                                child: ClipRRect(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(20)),
                                  child: BaseNetworkImage(
                                    url:
                                        '${state.product.home_store![index].picture}',
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Positioned(
                                top: 23,
                                left: 30,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    state.product.home_store![index].is_new ==
                                            true
                                        ? Container(
                                            height: 27,
                                            width: 27,
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                                color: const Color.fromRGBO(
                                                  255,
                                                  110,
                                                  78,
                                                  1,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(50)),
                                            child: const Text(
                                              'New',
                                              style: TextStyle(
                                                  fontFamily: 'Mark Pro',
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.w700,
                                                  color: Color.fromRGBO(
                                                    255,
                                                    255,
                                                    255,
                                                    1,
                                                  )),
                                            ),
                                          )
                                        : const SizedBox(
                                            height: 27,
                                            width: 27,
                                          ),
                                    const SizedBox(height: 18),
                                    Text(
                                      '${state.product.home_store![index].title}',
                                      style: const TextStyle(
                                          fontFamily: 'Mark Pro',
                                          fontSize: 25,
                                          fontWeight: FontWeight.w700,
                                          color: Color.fromRGBO(
                                            255,
                                            255,
                                            255,
                                            1,
                                          )),
                                    ),
                                    const SizedBox(height: 5),
                                    Text(
                                      '${state.product.home_store![index].subtitle}',
                                      style: const TextStyle(
                                          fontFamily: 'Mark Pro',
                                          fontSize: 11,
                                          fontWeight: FontWeight.w400,
                                          color: Color.fromRGBO(
                                            255,
                                            255,
                                            255,
                                            1,
                                          )),
                                    ),
                                    const SizedBox(height: 25),
                                    InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: ((context) =>
                                                    const ProductDetailPage())));
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: 23,
                                        width: 93,
                                        decoration: BoxDecoration(
                                            color: const Color.fromRGBO(
                                              255,
                                              255,
                                              255,
                                              1,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: const Text(
                                          'Buy now!',
                                          style: TextStyle(
                                              fontFamily: 'Mark Pro',
                                              fontSize: 11,
                                              fontWeight: FontWeight.w700,
                                              color:
                                                  Color.fromRGBO(1, 0, 53, 1)),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          );
                        },
                      );
                    } else {
                      return const SizedBox();
                    }
                  },
                ),
              )),
        ),
      ],
    );
  }
}
