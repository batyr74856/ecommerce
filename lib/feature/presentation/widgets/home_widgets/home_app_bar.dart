import 'package:ecommerce/core/app_assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeAppBar extends StatelessWidget {
  HomeAppBar({
    Key? key,
  }) : super(key: key);
  List<DropdownMenuItem<String>> get dropdownBrand {
    List<DropdownMenuItem<String>> menuItems = [
      const DropdownMenuItem(value: "Samsung", child: Text("Samsung")),
      const DropdownMenuItem(value: "Iphone", child: Text("Iphone")),
    ];
    return menuItems;
  }

  List<DropdownMenuItem<String>> get dropdownPrice {
    List<DropdownMenuItem<String>> menuItems = [
      const DropdownMenuItem(
          value: "\$500-\$1000", child: Text("\$500-\$1000")),
      const DropdownMenuItem(
          value: "\$1000-\$2000", child: Text("\$1000-\$2000")),
    ];
    return menuItems;
  }

  List<DropdownMenuItem<String>> get dropdownSize {
    List<DropdownMenuItem<String>> menuItems = [
      const DropdownMenuItem(
          value: "4.5 to 5.5 inches", child: Text("4.5 to 5.5 inches")),
      const DropdownMenuItem(
          value: "5.5 to 6.0 inches", child: Text("5.5 to 6.0 inches")),
    ];
    return menuItems;
  }

  String? selectedBrand;
  String? selectedSize;
  String? selectedPrice;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: const Color.fromRGBO(248, 248, 248, 1),
      title: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(
              width: 10,
            ),
            Row(
              children: [
                SvgPicture.asset(AppAssets.location),
                const SizedBox(
                  width: 5,
                ),
                const Text(
                  'Zihuatanejo, Gro',
                  style: TextStyle(
                      fontFamily: 'Mark Pro',
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(
                        1,
                        0,
                        53,
                        1,
                      )),
                ),
                const SizedBox(
                  width: 5,
                ),
                SvgPicture.asset(AppAssets.arrowDown),
              ],
            ),
            InkWell(
                onTap: () {
                  _modalBottomSheetMenu(context);
                },
                child: SvgPicture.asset(AppAssets.filter)),
          ],
        ),
      ),
    );
  }

  void _modalBottomSheetMenu(BuildContext context) {
    showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15), topRight: Radius.circular(15)),
        ),
        context: context,
        builder: (builder) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return Container(
                height: 375,
                decoration: const BoxDecoration(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15))),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              padding: const EdgeInsets.all(12),
                              height: 37,
                              width: 37,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: const Color.fromRGBO(
                                    1,
                                    0,
                                    53,
                                    1,
                                  )),
                              child: SvgPicture.asset(AppAssets.close),
                            ),
                          ),
                          const Text(
                            'Filter options',
                            style: TextStyle(
                                fontFamily: 'Mark Pro',
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Color.fromRGBO(
                                  1,
                                  0,
                                  53,
                                  1,
                                )),
                          ),
                          Container(
                            alignment: Alignment.center,
                            height: 37,
                            width: 70,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: const Color.fromRGBO(
                                  255,
                                  110,
                                  78,
                                  1,
                                )),
                            child: const Text(
                              'Done',
                              style: TextStyle(
                                  fontFamily: 'Mark Pro',
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: const [
                          Text(
                            'Brand',
                            style: TextStyle(
                                fontFamily: 'Mark Pro',
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: Color.fromRGBO(1, 0, 53, 1)),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      DropdownButtonFormField(
                        dropdownColor: Colors.white,
                        icon: SvgPicture.asset(
                          'assets/icons/arrow_down.svg',
                          width: 10,
                        ),
                        isExpanded: true,
                        hint: const Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Samsung',
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w300,
                                color: Color.fromRGBO(1, 0, 53, 1)),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        alignment: Alignment.center,
                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(1, 0, 53, 1)),
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 15),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  width: 1, color: Color.fromRGBO(1, 0, 53, 1)),
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              borderSide: const BorderSide(
                                  width: 1,
                                  color: Color.fromRGBO(220, 220, 220, 1)),
                            ),
                            filled: true,
                            fillColor: const Color.fromRGBO(
                              255,
                              255,
                              255,
                              1,
                            )),
                        items: dropdownBrand,
                        onChanged: (String? newValue) {
                          setState(() {
                            selectedBrand = newValue!;
                          });
                        },
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: const [
                          Text(
                            'Price',
                            style: TextStyle(
                                fontFamily: 'Mark Pro',
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: Color.fromRGBO(1, 0, 53, 1)),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      DropdownButtonFormField(
                        dropdownColor: Colors.white,
                        icon: SvgPicture.asset(
                          'assets/icons/arrow_down.svg',
                          width: 10,
                        ),
                        isExpanded: true,
                        hint: const Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '\$300-\$500',
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w300,
                                color: Color.fromRGBO(1, 0, 53, 1)),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        alignment: Alignment.center,
                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(1, 0, 53, 1)),
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 15),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  width: 1, color: Color.fromRGBO(1, 0, 53, 1)),
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              borderSide: const BorderSide(
                                  width: 1,
                                  color: Color.fromRGBO(220, 220, 220, 1)),
                            ),
                            filled: true,
                            fillColor: const Color.fromRGBO(
                              255,
                              255,
                              255,
                              1,
                            )),
                        items: dropdownPrice,
                        onChanged: (String? newValue) {
                          setState(() {
                            selectedPrice = newValue!;
                          });
                        },
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: const [
                          Text(
                            'Size',
                            style: TextStyle(
                                fontFamily: 'Mark Pro',
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: Color.fromRGBO(1, 0, 53, 1)),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      DropdownButtonFormField(
                        dropdownColor: Colors.white,
                        icon: SvgPicture.asset(
                          'assets/icons/arrow_down.svg',
                          width: 10,
                        ),
                        isExpanded: true,
                        hint: const Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '4.5 to 5.5 inches',
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w300,
                                color: Color.fromRGBO(1, 0, 53, 1)),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        alignment: Alignment.center,
                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(1, 0, 53, 1)),
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 15),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  width: 1, color: Color.fromRGBO(1, 0, 53, 1)),
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              borderSide: const BorderSide(
                                  width: 1,
                                  color: Color.fromRGBO(220, 220, 220, 1)),
                            ),
                            filled: true,
                            fillColor: const Color.fromRGBO(
                              255,
                              255,
                              255,
                              1,
                            )),
                        items: dropdownSize,
                        onChanged: (String? newValue) {
                          setState(() {
                            selectedSize = newValue!;
                          });
                        },
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }
}
