import 'package:ecommerce/core/app_assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 25,
      ),
      child: Row(
        children: [
          Expanded(
            child: TextFormField(
              textCapitalization: TextCapitalization.sentences,
              cursorColor: const Color.fromRGBO(1, 0, 53, 1),
              style: const TextStyle(
                fontFamily: 'Mark Pro',
                fontSize: 12,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(1, 0, 53, 1),
              ),
              textAlign: TextAlign.left,
              decoration: InputDecoration(
                  prefixIcon: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: SvgPicture.asset(
                      AppAssets.search,
                    ),
                  ),
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 24),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                        width: 1,
                        color: Color.fromRGBO(
                          255,
                          110,
                          78,
                          1,
                        )),
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                  hintText: 'Search',
                  hintStyle: const TextStyle(
                    fontFamily: 'Mark Pro',
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(1, 0, 53, 1),
                  ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50.0),
                      borderSide: BorderSide.none),
                  filled: true,
                  fillColor: const Color.fromRGBO(255, 255, 255, 1)),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
                color: const Color.fromRGBO(
                  255,
                  110,
                  78,
                  1,
                ),
                borderRadius: BorderRadius.circular(50)),
            child: SvgPicture.asset(AppAssets.qr),
          )
        ],
      ),
    );
  }
}
