import 'package:ecommerce/core/image_network.dart';
import 'package:ecommerce/feature/presentation/bloc/product_bloc/product_bloc.dart';

import 'package:ecommerce/feature/presentation/pages/product_detail_screen.dart';
import 'package:ecommerce/service_locator.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'is_favorite.dart';

class BestSellerWidget extends StatefulWidget {
  const BestSellerWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<BestSellerWidget> createState() => _BestSellerWidgetState();
}

class _BestSellerWidgetState extends State<BestSellerWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 25,
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    'Best Seller',
                    style: TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 25,
                        fontWeight: FontWeight.w700,
                        color: Color.fromRGBO(
                          1,
                          0,
                          53,
                          1,
                        )),
                  ),
                  Text(
                    'see more',
                    style: TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.white),
                  ),
                ])),
        BlocProvider<ProductBloc>(
          create: (context) => serviceLocator<ProductBloc>()..add(GetProduct()),
          child: BlocBuilder<ProductBloc, ProductState>(
            builder: (context, state) {
              if (state is ProductInitial) {
                return const SizedBox();
              } else if (state is ProductLoading) {
                return const Center(
                    child: CircularProgressIndicator(color: Colors.white));
              } else if (state is ProductLoaded) {
                return Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 25,
                  ),
                  child: GridView.builder(
                      physics: const ScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.product.best_seller!.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              crossAxisSpacing: 12,
                              childAspectRatio: 4 / 5,
                              mainAxisSpacing: 12),
                      itemBuilder: (context, index) {
                        return Stack(
                          alignment: Alignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: ((context) =>
                                            const ProductDetailPage())));
                              },
                              child: Container(
                                height: 250,
                                width: 181,
                                decoration: BoxDecoration(
                                    color:
                                        const Color.fromRGBO(255, 255, 255, 1),
                                    borderRadius: BorderRadius.circular(12)),
                                child: Column(
                                  children: [
                                    const SizedBox(
                                      height: 4,
                                    ),
                                    SizedBox(
                                      height: 140,
                                      width: 130,
                                      child: BaseNetworkImage(
                                        url: state.product.best_seller![index]
                                            .picture,
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 22),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Text(
                                                '\$ ${state.product.best_seller![index].price_without_discount}',
                                                style: const TextStyle(
                                                    fontFamily: 'Mark Pro',
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.w700,
                                                    color: Color.fromRGBO(
                                                      1,
                                                      0,
                                                      53,
                                                      1,
                                                    )),
                                              ),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                '\$${state.product.best_seller![index].discount_price}',
                                                style: const TextStyle(
                                                    decoration: TextDecoration
                                                        .lineThrough,
                                                    fontFamily: 'Mark Pro',
                                                    fontSize: 10,
                                                    fontWeight: FontWeight.w700,
                                                    color: Color.fromRGBO(
                                                      204,
                                                      204,
                                                      204,
                                                      1,
                                                    )),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            ' ${state.product.best_seller![index].title}',
                                            style: const TextStyle(
                                                fontFamily: 'Mark Pro',
                                                fontSize: 10,
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromRGBO(
                                                  1,
                                                  0,
                                                  53,
                                                  1,
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                                top: 6,
                                right: 8,
                                child: IsFavorite(
                                  title:
                                      state.product.best_seller![index].title,
                                  price: state.product.best_seller![index]
                                      .price_without_discount,
                                  image:
                                      state.product.best_seller![index].picture,
                                )),
                          ],
                        );
                      }),
                );
              } else {
                return const SizedBox();
              }
            },
          ),
        ),
      ],
    );
  }
}
