import 'package:ecommerce/core/app_assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

// ignore: must_be_immutable
class SelectCategoryWidget extends StatefulWidget {
  SelectCategoryWidget({Key? key, required this.activeTab}) : super(key: key);
  int activeTab = 0;

  @override
  State<SelectCategoryWidget> createState() => _SelectCategoryWidgetState();
}

class _SelectCategoryWidgetState extends State<SelectCategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 4),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    'Select Category',
                    style: TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 25,
                        fontWeight: FontWeight.w700,
                        color: Color.fromRGBO(
                          1,
                          0,
                          53,
                          1,
                        )),
                  ),
                  Text(
                    'view all',
                    style: TextStyle(
                        fontFamily: 'Mark Pro',
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(
                          255,
                          110,
                          78,
                          1,
                        )),
                  ),
                ])),
        const SizedBox(
          height: 20,
        ),
        SizedBox(
          height: 100,
          child: ListView(
              padding: const EdgeInsets.only(left: 25),
              physics: const AlwaysScrollableScrollPhysics(),
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 23),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        widget.activeTab = 0;
                      });
                    },
                    child: Column(
                      children: [
                        Ink(
                            height: 71,
                            width: 71,
                            padding: const EdgeInsets.symmetric(
                                vertical: 20, horizontal: 25),
                            decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(50)),
                                color: widget.activeTab == 0
                                    ? const Color.fromRGBO(
                                        255,
                                        110,
                                        78,
                                        1,
                                      )
                                    : const Color.fromRGBO(255, 255, 255, 1)),
                            child: SvgPicture.asset(
                              AppAssets.phones,
                              color: widget.activeTab == 0
                                  ? const Color.fromRGBO(
                                      255,
                                      255,
                                      255,
                                      1,
                                    )
                                  : const Color.fromRGBO(179, 179, 195, 1),
                            )),
                        const SizedBox(
                          height: 7,
                        ),
                        Text(
                          'Phones',
                          style: TextStyle(
                            fontFamily: 'Mark Pro',
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: widget.activeTab == 0
                                ? const Color.fromRGBO(
                                    255,
                                    110,
                                    78,
                                    1,
                                  )
                                : const Color.fromRGBO(1, 0, 53, 1),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 23),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        widget.activeTab = 1;
                      });
                    },
                    child: Column(
                      children: [
                        Ink(
                            height: 71,
                            width: 71,
                            padding: const EdgeInsets.symmetric(
                                vertical: 20, horizontal: 25),
                            decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(50)),
                                color: widget.activeTab == 1
                                    ? const Color.fromRGBO(
                                        255,
                                        110,
                                        78,
                                        1,
                                      )
                                    : const Color.fromRGBO(255, 255, 255, 1)),
                            child: SvgPicture.asset(
                              AppAssets.computer,
                              color: widget.activeTab == 1
                                  ? const Color.fromRGBO(
                                      255,
                                      255,
                                      255,
                                      1,
                                    )
                                  : const Color.fromRGBO(179, 179, 195, 1),
                            )),
                        const SizedBox(
                          height: 7,
                        ),
                        Text(
                          'Computer',
                          style: TextStyle(
                            fontFamily: 'Mark Pro',
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: widget.activeTab == 1
                                ? const Color.fromRGBO(
                                    255,
                                    110,
                                    78,
                                    1,
                                  )
                                : const Color.fromRGBO(1, 0, 53, 1),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 23),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        widget.activeTab = 2;
                      });
                    },
                    child: Column(
                      children: [
                        Ink(
                            height: 71,
                            width: 71,
                            padding: const EdgeInsets.symmetric(
                                vertical: 20, horizontal: 25),
                            decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(50)),
                                color: widget.activeTab == 2
                                    ? const Color.fromRGBO(
                                        255,
                                        110,
                                        78,
                                        1,
                                      )
                                    : const Color.fromRGBO(255, 255, 255, 1)),
                            child: SvgPicture.asset(
                              AppAssets.health,
                              color: widget.activeTab == 2
                                  ? const Color.fromRGBO(
                                      255,
                                      255,
                                      255,
                                      1,
                                    )
                                  : const Color.fromRGBO(1, 0, 53, 0.7),
                            )),
                        const SizedBox(
                          height: 7,
                        ),
                        Text(
                          'Health',
                          style: TextStyle(
                            fontFamily: 'Mark Pro',
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: widget.activeTab == 2
                                ? const Color.fromRGBO(
                                    255,
                                    110,
                                    78,
                                    1,
                                  )
                                : const Color.fromRGBO(1, 0, 53, 1),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 23),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        widget.activeTab = 3;
                      });
                    },
                    child: Column(
                      children: [
                        Ink(
                            height: 71,
                            width: 71,
                            padding: const EdgeInsets.symmetric(
                                vertical: 20, horizontal: 25),
                            decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(50)),
                                color: widget.activeTab == 3
                                    ? const Color.fromRGBO(
                                        255,
                                        110,
                                        78,
                                        1,
                                      )
                                    : const Color.fromRGBO(255, 255, 255, 1)),
                            child: SvgPicture.asset(
                              AppAssets.books,
                              color: widget.activeTab == 3
                                  ? const Color.fromRGBO(
                                      255,
                                      255,
                                      255,
                                      1,
                                    )
                                  : const Color.fromRGBO(179, 179, 195, 1),
                            )),
                        const SizedBox(
                          height: 7,
                        ),
                        Text(
                          'Books',
                          style: TextStyle(
                            fontFamily: 'Mark Pro',
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: widget.activeTab == 3
                                ? const Color.fromRGBO(
                                    255,
                                    110,
                                    78,
                                    1,
                                  )
                                : const Color.fromRGBO(1, 0, 53, 1),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 23),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        widget.activeTab = 4;
                      });
                    },
                    child: Column(
                      children: [
                        Ink(
                          height: 71,
                          width: 71,
                          padding: const EdgeInsets.symmetric(
                              vertical: 20, horizontal: 25),
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(50)),
                              color: widget.activeTab == 4
                                  ? const Color.fromRGBO(
                                      255,
                                      110,
                                      78,
                                      1,
                                    )
                                  : const Color.fromRGBO(255, 255, 255, 1)),
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        Text(
                          'Other',
                          style: TextStyle(
                            fontFamily: 'Mark Pro',
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: widget.activeTab == 4
                                ? const Color.fromRGBO(
                                    255,
                                    110,
                                    78,
                                    1,
                                  )
                                : const Color.fromRGBO(1, 0, 53, 1),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ]),
        ),
      ],
    );
  }
}
