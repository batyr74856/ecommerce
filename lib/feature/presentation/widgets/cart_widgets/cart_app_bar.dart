import 'package:ecommerce/core/app_assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppBarWidget extends StatelessWidget {
  const AppBarWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: const EdgeInsets.all(12),
              height: 37,
              width: 37,
              decoration: BoxDecoration(
                  color: const Color.fromRGBO(1, 0, 53, 1),
                  borderRadius: BorderRadius.circular(10)),
              child: SvgPicture.asset(AppAssets.arrowBack),
            ),
          ),
          Row(
            children: [
              const Text(
                'Add address',
                style: TextStyle(
                    fontFamily: 'Mark Pro',
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: Color.fromRGBO(
                      1,
                      0,
                      53,
                      1,
                    )),
              ),
              const SizedBox(
                width: 9,
              ),
              Container(
                padding: const EdgeInsets.all(10),
                height: 37,
                width: 37,
                decoration: BoxDecoration(
                    color: const Color.fromRGBO(255, 110, 78, 1),
                    borderRadius: BorderRadius.circular(10)),
                child: SvgPicture.asset(
                  AppAssets.location,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
