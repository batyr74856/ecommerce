import 'package:ecommerce/core/image_network.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../../core/app_assets.dart';
import '../../../../service_locator.dart';
import '../../bloc/cart_bloc/cart_bloc.dart';

class CartList extends StatelessWidget {
  const CartList({
    Key? key,
    required this.amount,
  }) : super(key: key);

  final int amount;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CartBloc>(
      create: (context) => serviceLocator<CartBloc>()..add(GetCartEvent()),
      child: BlocBuilder<CartBloc, CartState>(
        builder: (context, state) {
          if (state is CartInitial) {
            return const SizedBox();
          } else if (state is CartLoading) {
            return const Center(
                child: CircularProgressIndicator(
                    color: Color.fromRGBO(
              255,
              110,
              78,
              1,
            )));
          } else if (state is CartLoaded) {
            return ListView.builder(
              shrinkWrap: true,
              itemCount: state.cart.basket!.length,
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 95,
                        width: 89,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: BaseNetworkImage(
                            url: state.cart.basket![index].images,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 155,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              state.cart.basket![index].title!,
                              style: const TextStyle(
                                  fontFamily: 'Mark Pro',
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                            Text(
                              '\$${state.cart.basket![index].price!}.00',
                              style: const TextStyle(
                                  fontFamily: 'Mark Pro',
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                  color: Color.fromRGBO(
                                    255,
                                    110,
                                    78,
                                    1,
                                  )),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        child: Row(children: [
                          Container(
                            height: 75,
                            width: 26,
                            decoration: BoxDecoration(
                                color: const Color.fromRGBO(
                                  40,
                                  40,
                                  67,
                                  1,
                                ),
                                borderRadius: BorderRadius.circular(26)),
                            child: Column(
                              children: [
                                const Text(
                                  '-',
                                  style: TextStyle(
                                      fontFamily: 'Mark Pro',
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                ),
                                Text(
                                  '',
                                  style: const TextStyle(
                                      fontFamily: 'Mark Pro',
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white),
                                ),
                                const Text(
                                  '+',
                                  style: TextStyle(
                                      fontFamily: 'Mark Pro',
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          SvgPicture.asset(AppAssets.delete)
                        ]),
                      )
                    ],
                  ),
                );
              },
            );
          } else {
            return const SizedBox();
          }
        },
      ),
    );
  }
}
