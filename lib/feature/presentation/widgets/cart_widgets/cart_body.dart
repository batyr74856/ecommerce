import 'package:flutter/material.dart';

import 'cart_list.dart';
import 'cart_total.dart';
import 'checkout_button.dart';

class CartBody extends StatelessWidget {
  const CartBody({
    Key? key,
    required this.amount,
  }) : super(key: key);

  final int amount;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 550,
        decoration: const BoxDecoration(
            color: Color.fromRGBO(
              1,
              0,
              53,
              1,
            ),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            CartList(amount: amount),
            const SizedBox(
              height: 50,
            ),
            const Divider(
              thickness: 2,
              indent: 5,
              endIndent: 5,
              color: Color.fromRGBO(
                255,
                255,
                255,
                0.25,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            const CartTotal(),
            const SizedBox(
              height: 20,
            ),
            const Divider(
              thickness: 1,
              indent: 5,
              endIndent: 5,
              color: Color.fromRGBO(
                255,
                255,
                255,
                0.2,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const CheckoutButton(),
          ],
        ),
      ),
    );
  }
}
