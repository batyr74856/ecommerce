import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/app_enums.dart';

part 'nav_state.dart';

class NavCubit extends Cubit<NavState> {
  NavCubit() : super(const NavInitial());

  void changeNav(BottomNavIndex newIndex) {
    emit(NavChanged(newIndex));
  }
}
