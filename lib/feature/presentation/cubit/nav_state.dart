part of 'nav_cubit.dart';

abstract class NavState extends Equatable {
  final BottomNavIndex navIndex;

  const NavState(this.navIndex);

  @override
  List<Object?> get props => [navIndex];
}

class NavInitial extends NavState {
  const NavInitial() : super(BottomNavIndex.home);
}

class NavChanged extends NavState {
  final BottomNavIndex newIndex;

  const NavChanged(this.newIndex) : super(newIndex);
}
