import 'dart:convert';

import '../../models/product.dart';
import '../../../../utils/shared_preference.dart';

class FavoriteProvider {
  static List<BestSeller> favorites = [];

  Future<void> fetchFromLocalStorage() async {
    await sharedPreference.reload();
    String favorite = sharedPreference.favorite ?? '[]';
    favorites = (json.decode(favorite) as List? ?? [])
        .map((e) => BestSeller.fromJson(e))
        .toList();
  }

  Future<void> saveFavorite(
      String title, image, int price, bool isFavorite) async {
    await fetchFromLocalStorage();
    favorites.add(BestSeller(
        title: title,
        picture: image,
        price_without_discount: price,
        isFavorites: isFavorite));

    sharedPreference.saveFavorite(json.encode(favorites));
  }

  Future<void> removeFavorite(
    String title,
    image,
    int price,
    bool isFavorite,
  ) async {
    await fetchFromLocalStorage();
    favorites.remove(BestSeller(
        title: title,
        picture: image,
        price_without_discount: price,
        isFavorites: isFavorite));

    sharedPreference.removeFavorite(json.encode(favorites));
  }
}
