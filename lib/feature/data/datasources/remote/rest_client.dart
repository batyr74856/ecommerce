import 'package:dio/dio.dart';
import 'package:ecommerce/feature/data/models/cart.dart';
import 'package:ecommerce/feature/data/models/product.dart';
import 'package:ecommerce/feature/data/models/product_detail.dart';

import 'package:retrofit/http.dart';

import '../../../../core/env.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: apiURL)
abstract class RestClient {
  factory RestClient(Dio dio, {String? baseUrl}) = _RestClient;

  @GET('/v3/654bd15e-b121-49ba-a588-960956b15175')
  Future<Product> getProducts();

  @GET('/v3/6c14c560-15c6-4248-b9d2-b4508df7d4f5')
  Future<ProductDetail> getProductDetail();

  @GET('/v3/53539a72-3c5f-4f30-bbb1-6ca10d42c149')
  Future<Cart> getCart();
}
