import 'package:json_annotation/json_annotation.dart';

part 'product.g.dart';

@JsonSerializable()
class Product {
  // ignore: non_constant_identifier_names
  List<HomeStore>? home_store;
  // ignore: non_constant_identifier_names
  List<BestSeller>? best_seller;
  // ignore: non_constant_identifier_names
  Product({this.home_store, this.best_seller});

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}

@JsonSerializable()
class HomeStore {
  int? id;
  // ignore: non_constant_identifier_names
  bool? is_new;
  String? title;
  String? subtitle;
  String? picture;
  // ignore: non_constant_identifier_names
  bool? is_buy;

  HomeStore(
      {this.id,
      // ignore: non_constant_identifier_names
      this.is_new,
      this.title,
      this.subtitle,
      this.picture,
      // ignore: non_constant_identifier_names
      this.is_buy});
  factory HomeStore.fromJson(Map<String, dynamic> json) =>
      _$HomeStoreFromJson(json);

  Map<String, dynamic> toJson() => _$HomeStoreToJson(this);
}

@JsonSerializable()
class BestSeller {
  int? id;
  bool? isFavorites;
  String? title;
  // ignore: non_constant_identifier_names
  int? price_without_discount;
  // ignore: non_constant_identifier_names
  int? discount_price;
  String? picture;

  BestSeller(
      {this.id,
      this.isFavorites,
      this.title,
      // ignore: non_constant_identifier_names
      this.price_without_discount,
      // ignore: non_constant_identifier_names
      this.discount_price,
      this.picture});
  factory BestSeller.fromJson(Map<String, dynamic> json) =>
      _$BestSellerFromJson(json);

  Map<String, dynamic> toJson() => _$BestSellerToJson(this);
}
