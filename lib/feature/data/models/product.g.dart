// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) => Product(
      home_store: (json['home_store'] as List<dynamic>?)
          ?.map((e) => HomeStore.fromJson(e as Map<String, dynamic>))
          .toList(),
      best_seller: (json['best_seller'] as List<dynamic>?)
          ?.map((e) => BestSeller.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'home_store': instance.home_store,
      'best_seller': instance.best_seller,
    };

HomeStore _$HomeStoreFromJson(Map<String, dynamic> json) => HomeStore(
      id: json['id'] as int?,
      is_new: json['is_new'] as bool?,
      title: json['title'] as String?,
      subtitle: json['subtitle'] as String?,
      picture: json['picture'] as String?,
      is_buy: json['is_buy'] as bool?,
    );

Map<String, dynamic> _$HomeStoreToJson(HomeStore instance) => <String, dynamic>{
      'id': instance.id,
      'is_new': instance.is_new,
      'title': instance.title,
      'subtitle': instance.subtitle,
      'picture': instance.picture,
      'is_buy': instance.is_buy,
    };

BestSeller _$BestSellerFromJson(Map<String, dynamic> json) => BestSeller(
      id: json['id'] as int?,
      isFavorites: json['isFavorites'] as bool?,
      title: json['title'] as String?,
      price_without_discount: json['price_without_discount'] as int?,
      discount_price: json['discount_price'] as int?,
      picture: json['picture'] as String?,
    );

Map<String, dynamic> _$BestSellerToJson(BestSeller instance) =>
    <String, dynamic>{
      'id': instance.id,
      'isFavorites': instance.isFavorites,
      'title': instance.title,
      'price_without_discount': instance.price_without_discount,
      'discount_price': instance.discount_price,
      'picture': instance.picture,
    };
