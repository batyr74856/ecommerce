import 'package:json_annotation/json_annotation.dart';

part 'product_detail.g.dart';

@JsonSerializable()
class ProductDetail {
  // ignore: non_constant_identifier_names
  String? CPU;
  String? camera;
  List<String>? capacity;
  List<String>? color;
  String? id;
  List<String>? images;
  bool? isFavorites;
  int? price;
  double? rating;
  String? sd;
  String? ssd;
  String? title;

  ProductDetail(
      // ignore: non_constant_identifier_names
      {this.CPU,
      this.camera,
      this.capacity,
      this.color,
      this.id,
      this.images,
      this.isFavorites,
      this.price,
      this.rating,
      this.sd,
      this.ssd,
      this.title});

  factory ProductDetail.fromJson(Map<String, dynamic> json) =>
      _$ProductDetailFromJson(json);

  Map<String, dynamic> toJson() => _$ProductDetailToJson(this);
}
