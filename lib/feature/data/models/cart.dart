import 'package:json_annotation/json_annotation.dart';

part 'cart.g.dart';

@JsonSerializable()
class Cart {
  List<Basket>? basket;
  String? delivery;
  String? id;
  int? total;

  Cart({this.basket, this.delivery, this.id, this.total});

  factory Cart.fromJson(Map<String, dynamic> json) => _$CartFromJson(json);

  Map<String, dynamic> toJson() => _$CartToJson(this);
}

@JsonSerializable()
class Basket {
  int? id;
  String? images;
  int? price;
  String? title;
  Basket({this.id, this.images, this.price, this.title});

  factory Basket.fromJson(Map<String, dynamic> json) => _$BasketFromJson(json);

  Map<String, dynamic> toJson() => _$BasketToJson(this);
}
