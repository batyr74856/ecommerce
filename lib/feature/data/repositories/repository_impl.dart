import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:ecommerce/feature/data/datasources/remote/rest_client.dart';
import 'package:ecommerce/feature/data/models/product.dart';
import 'package:ecommerce/feature/data/models/product_detail.dart';

import '../../../service_locator.dart';
import '../models/cart.dart';

abstract class RepositoriesService {
  Future<Either<DioError, Product>> getProductList();
  Future<Either<DioError, ProductDetail>> getProductDetail();
  Future<Either<DioError, Cart>> getCart();
}

class ServiceImpl extends RepositoriesService {
  @override
  Future<Either<DioError, Cart>> getCart() async {
    try {
      return Right(await serviceLocator<RestClient>().getCart());
    } on DioError catch (e) {
      return Left(e);
    }
  }

  @override
  Future<Either<DioError, ProductDetail>> getProductDetail() async {
    try {
      return Right(await serviceLocator<RestClient>().getProductDetail());
    } on DioError catch (e) {
      return Left(e);
    }
  }

  @override
  Future<Either<DioError, Product>> getProductList() async {
    try {
      var response = await serviceLocator<RestClient>().getProducts();
      return Right(response);
    } on DioError catch (e) {
      return Left(e);
    }
  }
}
