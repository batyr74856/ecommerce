import 'package:ecommerce/feature/presentation/bloc/cart_bloc/cart_bloc.dart';

import 'package:ecommerce/feature/presentation/bloc/product_bloc/product_bloc.dart';

import 'package:ecommerce/feature/presentation/bloc/product_detail_bloc/product_detail_bloc.dart';

import 'package:ecommerce/utils/shared_preference.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'feature/presentation/cubit/nav_cubit.dart';
import 'feature/presentation/pages/splash_screen.dart';
import 'service_locator.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  serviceLocatorSetup();
  sharedPreference.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<NavCubit>(create: (context) => serviceLocator<NavCubit>()),
        BlocProvider<ProductBloc>(
            create: (context) => serviceLocator<ProductBloc>()),
        BlocProvider<ProductDetailBloc>(
            create: (context) => serviceLocator<ProductDetailBloc>()),
        BlocProvider<CartBloc>(create: (context) => serviceLocator<CartBloc>()),
      ],
      child: MaterialApp(
          title: 'Ecommerce',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            scaffoldBackgroundColor: Colors.white,
          ),
          home: const SplashScreen()),
    );
  }
}
