import 'package:shared_preferences/shared_preferences.dart';

final sharedPreference = SharedPreferenceHelper();

class SharedPreferenceHelper {
  late SharedPreferences _sharedPreference;

  // ignore: prefer_final_fields
  String _favorite = '_favorite';

  init() async {
    _sharedPreference = await SharedPreferences.getInstance();
  }

  Future<void> reload() async {
    await _sharedPreference.reload();
  }

  Future<void> saveFavorite(String favorite) async {
    _sharedPreference.setString(_favorite, favorite);
  }

  String? get favorite {
    return _sharedPreference.getString(_favorite) ?? '[]';
  }

  Future<void> removeFavorite(String favorite) async {
    _sharedPreference.clear();
  }
}
